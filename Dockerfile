FROM alpine:3.7
RUN apk update && apk add bash curl jq git dialog less
ADD gh-query /usr/bin/
ENTRYPOINT [ "/usr/bin/gh-query" ]
