What is it?
===========
`gh-query` is an interactive commandline utility which allows to navigate and review contents of public github gists. 
`gh-query` fetches all public gists of a requested github user and stores them locally.
On each subsequent run `gh-query` checks if there are any new gists available and highlights them in the output.

Installation
============
Docker is the recommended way of installing this software.

```
git clone https://gitlab.com/yaroslav.tarasenko/gh-query.git
cd gh-query
docker build -t gh-query .
```

Usage
=====
Add this snippet to your `.bashrc`:

```
gh() { 
  docker run -v $HOME/.gh-query:/root/.gh-query -ti gh-query $@ 
}
```

After reloading your shell, you can work with the tool by simply typing

```
gh <github-username>
```

For example:

```
gh vivus-ignis
```
